# jetbrains Docker Swarm Version
Jetbrains Server based on http://jetbrains.license.laucyun.com/


To deploy run:

docker run --name jetbrains -d -p 8000:8000 masterfox/jetbrains:latest

Deploy to SWARM:

docker stack deploy -c docker-compose.yml jetbrains-server
